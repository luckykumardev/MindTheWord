var http = require('http');
var fs = require('fs');
var jquery = require('jquery');
const translate = require('google-translate-api');
 
 

// Loading the index file . html displayed to the client
var server = http.createServer(function(req, res) {
    fs.readFile('./index.html', 'utf-8', function(error, content) {
        res.writeHead(200, {"Content-Type": "text/html"});
        res.end(content);
    });
});


// Loading socket.io
var io = require('socket.io').listen(server);

// When a client connects, we note it in the console
io.sockets.on('connection', function (socket) {

  
    // When the server receives a “message” type signal from the client   
    socket.on('message', function (message) {
        console.log('A client is speaking to me! They’re saying: ' + message);
    }); 



 socket.on('message', function (message) {
        //console.log('A client is speaking to me! They’re saying: ' + message);
    

      translate(message.text, {from: message.from_language, to: message.to_language}).then(res => {
        
    //=> Ik spreek Nederlands! 
          // console.log(res.from.text.autoCorrected);
    //=> true 
        //   console.log(res.from.text.value);
    //=> I [speak] Dutch! 
         //  console.log(res.from.text.didYouMean);
    //=> false 
            socket.emit('message', res.text);

           }).catch(err => {
          console.error(err);
         });

    });

});

server.listen(process.env.PORT || 5000, function(){
  console.log('listening on', server.address().port);
});